# logger

日志作为整个代码行为的记录，是程序执行逻辑和异常最直接的反馈，支持标准输出和高性能磁盘写入,多样化的配置方式，使用起来简单方便，5个日志级别满足项目中各种需求。

## 安装
```shell script
go get git.100tal.com/tal_jishuzhongtai_xiaoneng_go/logger
```

## 配置示例
```ini
[Log]
;日志输出文件
fileName = /home/logs/xeslog/consumer/consumer.log
;日志级别
level = DEBUG
;日志最大保存MB
maxSize = 200
;旧日志保留5个备份
maxBackups = 5
;最多保留30天日志 和MaxBackups参数配置1个就可以
MaxAge=0
;控制台输出
console = true
;SuffixEnable环境变量
suffixEnv = dev
;启用日志压缩
compress = true
```

### 初始化
```go
import (
    "git.100tal.com/tal_jishuzhongtai_xiaoneng_go/config"
    "git.100tal.com/tal_jishuzhongtai_xiaoneng_go/logger"
)

func main(){
    
        // 准备日志配置参数
        cfgMap := config.GetConfStringMap("Log")
    
        // 设置环境信息
    	logger.SetEnv("gray")
    	logger.SetName("testLogger")
    	logger.SetDepartment("udcXiaoNeng")
    	logger.SetVersion("logger-v1.0.0")
    	logConfig := NewConfig().SetConfigMap(cfgMap)
    
        // 初始化日志对象
    	logger.InitWithConfig(logConfig)    
    	defer logger.Sync()
    
        // 记录错误日志
        // 前3个参数必传：上下文, 日志标签, 日志正文
        // 从第4个参数开始，属于日志键值对。 可以为空，但必须成对传入. 切记！切记！！切记！！！
    	logger.Ex(ctx, "testTag", "我是一条测试日志....", "age", 30, "box", "wll")
}

```
> httpserver 与 rpcxserver 已集成好初始化日志中间件，直接调用即可。

## 使用示例
```go
// 前3个参数必传：上下文, 日志标签, 日志正文
// 从第4个参数开始，属于日志键值对。 可以为空，但必须成对传入. 切记！切记！！切记！！！
logger.Dx(ctx, "testTag", "我是一条测试日志....", "age", 30, "box", "wll")
logger.Ix(ctx, "testTag", "我是一条测试日志....", "age", 30, "box", "wll")
logger.Wx(ctx, "testTag", "我是一条测试日志....", "age", 30, "box", "wll")
logger.Ex(ctx, "testTag", "我是一条测试日志....", "age", 30, "box", "wll")
``` 
> 日志组件支持将日志标记为，外部显示字段，比如示例中的 age,box 字段都将已json属性的形式出现，便于后续分析。

### 日志内容示例
```json
{"x_level":"error","@timestamp":"2021-03-23T16:42:20.846+0800","x_caller":"/Users/wll/Go/src/tal_jishuzhongtai_xiaoneng_go/logger/logger.go:60","x_msg":"我是一条测试日志....","x_env":"gray","x_name":"testLogger","x_version":"logger-v1.0.0","x_department":"udcXiaoNeng","x_server_ip":"10.25.150.96","x_host_name":"WLL-For-Mac-mini.local","x_rpc_id":"5090909","x_trace_id":"1500960000000018","x_timestamp":1616488940,"x_duration":0.144,"x_tag":"testTag","age":30,"box":"望乐乐"}
```