module gitcode.com/common_go/logger

go 1.15

require (
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cast v1.3.1
	go.uber.org/zap v1.16.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)
