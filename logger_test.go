package logger

import (
	"context"
	"os/user"
	"testing"
	"time"
)

func TestIx(t *testing.T) {
	currUser, _ := user.Current()

	cfgMap := map[string]string{
		"fileName":  currUser.HomeDir + "/logs/udc/logger.log",
		"console":   "true",
		"level":     "DEBUG",
		"maxSize":   "200",
		"maxAge":    "0",
		"suffixEnv": "release",
		"compress":  "true",
		"localTime": "true",
	}

	SetJoinMod(true)

	// 初始化日志环境
	SetEnv("dev")
	SetName("testLogger")
	SetDepartment("udcXiaoNeng")
	SetVersion("logger-v1.0.0")
	logConfig := NewConfig().SetConfigMap(cfgMap)

	InitWithConfig(logConfig)

	defer Sync()

	ctx := context.Background()

	ctx = context.WithValue(ctx, "rpc_id", "udc_588588")
	ctx = context.WithValue(ctx, "start", time.Now())

	time.Sleep(1 * time.Second)

	Ix(ctx, "testTag", "我是一条测试日志....%v,%v,%v,%v", "age", 30, "box", "wanglele")

	time.Sleep(60 * time.Second)
}
